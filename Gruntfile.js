module.exports = function(grunt) {
    var dir = __dirname;
    grunt.initConfig({
        less: {
            build: {
                files: [
                    {
                        src: "stylesheet/main.less",
                        dest: "dist/main.css"
                    }
                ],
                paths: "stylesheet",
                compress: true,
                relativeUrls: true
            }
        },
        requirejs: {
            build: {
                options: {
                    baseUrl: "js",
                    mainConfigFile: "js/boot.js",
                    name: "app/main", // assumes a production build using almond
                    out: "dist/main.min.js",
                    paths:{
                        requireLib: 'bower_components/requirejs/require.js'
                    }
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-requirejs');

    grunt.registerTask('default', ['less:build', 'requirejs:build']);

};