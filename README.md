#Installation

To install the App you need [bower](http://bower.io/).

Then run <code>bower install</code> from the root directory.

After that you can open the index.html file (via a webserver, not as a file).

#Dev / Prod Environment

The App runs in two modes: Development and Production.

Development ist the default after installation. If you want to use the production environment, you need
to install the NPM packages (<code>npm install</code>), Grunt <code>npm install -g grunt</code> and then
run <code>grunt</code> in the document root.

The /dist/ folder will then be populated by main.min.js and main.css. To use these files, you need change the comments
in the index.html file. You will need to comment out the DEV Script and un-comment the PROD scripts.

The PROD scripts are just used as an example. there are a few optimizations missing, like cache busting,
minifying of both the CSS and the JS files and inlining the requirejs dependency.

#JS Structure

In general the app follows basic principles known as the MVC structure. The different parts mainly use events
to communicate between each other, which is standard practice in Javascript and allows for the "Realtime" feeling
without having to go overboard with manually updating everything in the controller.

In general there is some room for optimization by using inheritance to reduce duplicate code. In such a small app
that would result in more LOC, than just copying the code, so that's why it wasn't implemented in this project.

Naming things is hard. Most files could do with a better name.

##Entity Operations

Entities and Collections are managed by Backbone. You can find the files under the Collection or Model folder.

##Dependency Management

Bower is used as the package manager and requirejs is used to manage the dependencies inside the app. Keeping
track of dependencies and respecting the principle of single responsibility is key to keeping the app updatable.

##Views
A view is made up of two parts: The View definition in the Views folder and the respective HTML template in the
index.html file. In a bigger application the views could also be loaded via AJAX, but it's good practice to load
as many template as possible in the HTML page, since that reduces loading time while using the app.

The View definition under the Views folder defines (and in some cases delegates) the events that can occur in the view.
This allows for reuse of the view all over the application and sharing the meta information.

##Features that could implemented in the future:
- Validation of User Model (Should match server side validation)
- Sync Created and Edited entities back to source
- Optimize for JS Garbage Collection

#CSS/LESS Structure
Big frameworks have been avoided on purpose. The utility they would bring is too little for the bloated size of a framework in this case.
The design is based on Googles "Material Design". It doesn't follow it 100%, but it is based on.

The CSS classes are namespaced to avoid conflicts with possible classes from plugins.

Less is used as CSS pre-processor.

#Resources
##Styleguide Youos:
https://app.frontify.com/d/kyWeOXcvJ28r/youos-styleguide

##Visual Design App:
http://aweaked.com/youos/wIOel23pYwl/owner-stage-1.png
http://aweaked.com/youos/wIOel23pYwl/actor-stage-1.png
http://aweaked.com/youos/wIOel23pYwl/actor-stage-2.png
http://aweaked.com/youos/wIOel23pYwl/actor-stage-3.png
