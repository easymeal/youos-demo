define(['app/Collection/Users'], function(userCollection){
    return function(){
        var that = this;

        this.get = function(cb){
            var users = new userCollection();
            users.url = 'http://jsonplaceholder.typicode.com/users';
            users.fetch({
                success: cb,
                error: cb
            });
        }
    };
});