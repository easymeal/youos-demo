define(['backbone', 'app/Model/User'], function(Backbone, userModel){
    return Backbone.Collection.extend({
        model: userModel
    });
});