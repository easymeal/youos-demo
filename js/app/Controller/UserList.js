define([
    'app/Api/Users',
    'app/Model/User',
    'app/Model/Search',
    'app/Views/UserRow',
    'app/Views/UserDetail',
    'app/Views/UserEdit',
    'app/Views/UserCreate',
    'app/Views/Search',
    'app/Views/ConfirmDelete',
    'jquery'
], function(
    usersApiClass,
    userModel,
    searchModel,
    userRowView,
    userDetailView,
    userEditView,
    userCreateView,
    searchView,
    confirmDeleteView,
    $
){
    var usersApi = new usersApiClass;
    var userCollection;

    usersApi.get(function(collection){
        userCollection = collection;
        addUsersToList(collection);
        userCollection.on('update', addUsersToList);

        var flt = new searchModel({collection: userCollection});
        var inputView = new searchView({
            el: '.you-input-search',
            model: flt
        });
        flt.filtered.on('reset', addUsersToList.bind(flt.filtered))
    });


    function addUsersToList(collection){
        var wrapper = $('.you-wrapper');
        wrapper.html('');
        collection.toArray().forEach(function(userModel){
            var view = new userRowView({
                model: userModel
            });
            view.render();
            view.on('edit', showEditView);
            view.on('show', showDetailView);
            view.on('delete', showConfirmDeleteView);
            wrapper.append(view.$el);
        });
    }

    $('.you-button-create').on('click', function(e){
        showCreateView();
        e.preventDefault();
    });

    var showConfirmDeleteView = function(model){
        var confirm = new confirmDeleteView({
            model: model,
            callback: function(){
                userCollection.remove(this.model);
            }
        });
        $('body').append(confirm.$el);
    };

    var showCreateView = function(){
        var emptyUser = new userModel();
        var detailView = new userCreateView({
            model: emptyUser
        });
        $('body').append(detailView.$el);
        detailView.on('save', userCollection.add.bind(userCollection));
    };

    var showEditView = function(model){
        var detailView = new userEditView({
            model: model
        });
        $('body').append(detailView.$el);
    };

    var showDetailView = function(model){
        var detailView = new userDetailView({
            model: model
        });
        $('body').append(detailView.$el);
    };
});