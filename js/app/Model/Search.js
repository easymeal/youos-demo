define(['backbone'], function(Backbone){
    return Backbone.Model.extend({
        defaults: {
            what: '',
            where: 'all'
        },
        initialize: function(opts) {
            this.collection = opts.collection;
            this.filtered = new Backbone.Collection(opts.collection.models);
            this.on('change:what change:where', this.filter);
        },
        filter: function() {
            var what = this.get('what').trim(),
                where = this.get('where'),
                lookin = (where==='all') ? ['name', 'phone', 'email'] : where,
                models;

            if (what==='') {
                models = this.collection.models;
            } else {
                models = this.collection.filter(function(model) {
                    return _.some(_.values(model.pick(lookin)), function(value) {
                        return ~value.toLowerCase().indexOf(what.toLowerCase());
                    });
                });
            }

            this.filtered.reset(models);
        }
    });
});