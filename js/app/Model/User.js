define(['backbone'], function(Backbone){
    return Backbone.Model.extend({
        defaults: {
            name: "",
            phone: "",
            email: "",
            address: {
                street: "",
                suite: "",
                zipcode: "",
                city: ""
            }
        },
        initialize: function(){
            this.on('change:name', this.setInitials, this);
            this.setInitials();
        },
        setInitials: function(){
            var name = this.attributes.name;
            var nameParts = name.split(" ");
            var initials;
            if(nameParts.length > 1){
                var firstName = nameParts.shift();
                var secondName = nameParts.shift();
                initials = firstName.substr(0, 1) + secondName.substr(0, 1);
            }else{
                initials = '';
            }
            this.set('initials', initials);
        }
    });
});