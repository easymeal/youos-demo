define(['backbone', 'underscore'], function(Backbone, _){
    return Backbone.View.extend({
        events: {
            'click .you-close': 'close',
            'click .delete': 'confirm'
        },
        template: _.template(document.getElementById('confirm-delete').innerHTML),
        initialize: function(opts){
            this.render();
            this.callback = opts.callback;
        },
        render: function(){
            this.$el.html(this.template(this.model.attributes));
            return this;
        },
        close: function(){
            this.remove();
        },
        confirm: function(){
            this.callback();
            this.close();
        }
    });
});