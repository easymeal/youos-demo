define(['backbone'], function(Backbone){
    return Backbone.View.extend({
        events: {
            'keyup': _.throttle(function(e) {
                this.model.set('what', e.currentTarget.value);
            }, 200)
        }
    });
});