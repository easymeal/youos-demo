define(['backbone', 'underscore'], function(Backbone, _){
    return Backbone.View.extend({
        events: {
            'click .you-close': 'close',
            'submit .you-form-edit': 'saveModel'
        },
        template: _.template(document.getElementById('user-edit-template').innerHTML),
        initialize: function(){
            this.render();
        },
        render: function(){
            this.$el.html(this.template(this.model.attributes));
            return this;
        },
        close: function(){
            this.remove();
        },
        saveModel: function(){
            var formValues = this.$el.find('form').serializeArray();
            var model = this.model;
            formValues.forEach(function(field){
                model.set(field.name, field.value);
            });
            this.trigger('save', model);
            this.close();
        }
    });
});