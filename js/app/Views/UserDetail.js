define(['backbone', 'underscore'], function(Backbone, _){
    return Backbone.View.extend({
        events: {
            'click .you-close': 'close'
        },
        template: _.template(document.getElementById('user-detail-template').innerHTML),
        initialize: function(){
            this.model.on("change", this.render, this);
            this.render();
        },
        render: function(){
            this.$el.html(this.template(this.model.attributes));
            return this;
        },
        close: function(){
            this.remove();
        }
    });
});