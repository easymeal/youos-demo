define(['backbone', 'underscore'], function(Backbone, _){
    return Backbone.View.extend({
        events: {
            'click .you-close': 'close',
            'submit .you-form-edit': 'updateModel'
        },
        template: _.template(document.getElementById('user-edit-template').innerHTML),
        initialize: function(){
            this.model.on("change", this.render, this);
            this.render();
        },
        render: function(){
            this.$el.html(this.template(this.model.attributes));
            return this;
        },
        close: function(){
            this.remove();
        },
        updateModel: function(){
            var formValues = this.$el.find('form').serializeArray();
            var model = this.model;
            formValues.forEach(function(field){
                model.set(field.name, field.value);
            });
            this.close();
        }
    });
});