define(['backbone', 'underscore'], function(Backbone, _){
    return Backbone.View.extend({
        events: {
            'click .edit': 'edit',
            'click .delete': 'delete',
            'click .you-list-entry': 'show'
        },
        template: _.template(document.getElementById('user-list-row-template').innerHTML),
        initialize: function(){
            this.model.on("change", this.render, this);
            this.render();
        },
        render: function(){
            this.$el.html(this.template(this.model.attributes));
            return this;
        },
        edit: function(e){
            e.stopPropagation();
            this.trigger('edit', this.model);
        },
        show: function(){
            this.trigger('show', this.model);
        },
        delete: function(e){
            e.stopPropagation();
            this.trigger('delete', this.model);
        }
    });
});