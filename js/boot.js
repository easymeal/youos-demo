require.config({
  shim: {

  },
  paths: {
    requirejs: "../bower_components/requirejs/require",
    backbone: "../bower_components/backbone/backbone",
    jquery: "../bower_components/jquery/dist/jquery",
    app: "app",
    underscore: "../bower_components/underscore/underscore"
  },
  packages: [

  ]
});

require(['app/main']);